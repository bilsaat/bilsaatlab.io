const fullscreenToggle = document.getElementById('fullscreen-toggle');
   fullscreenToggle.addEventListener('change', () => {
    if (fullscreenToggle.checked) {
      document.documentElement.requestFullscreen();
    } else {
       document.exitFullscreen();
     }
  });