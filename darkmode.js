document.addEventListener('DOMContentLoaded', function () {
    // Check the local storage for the saved theme
    let savedTheme = JSON.parse(localStorage.getItem('PageTheme'));
    if (savedTheme === 'dark') {
      document.body.classList.add('dark-mode');
      document.getElementById('darkModeToggle').checked = true;
    }
  
    // Add event listener to the checkbox
    document.getElementById('darkModeToggle').addEventListener('change', function () {
      toggleDarkMode();
    });
  
    function toggleDarkMode() {
      var SetTheme = document.body;
      SetTheme.classList.toggle('dark-mode');
      var theme = SetTheme.classList.contains('dark-mode') ? 'dark' : 'light';
      localStorage.setItem('PageTheme', JSON.stringify(theme));
    }
  });
  