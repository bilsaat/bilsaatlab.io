// Function to update and display the countdown and messages on the webpage
        function updateCountdownWithMessage(targetHour, targetMinute, message, additionalMessage) {
            const currentTime = new Date();
            const targetTime = new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), targetHour, targetMinute);
            const timeDiff = targetTime - currentTime;

            if (timeDiff > 0) {
                const hoursRemaining = Math.floor(timeDiff / (1000 * 60 * 60));
                const minutesRemaining = Math.floor((timeDiff % (1000 * 60 * 60)) / (1000 * 60));
                const secondsRemaining = Math.floor((timeDiff % (1000 * 60)) / 1000);

                let countdownMessage = `${message} `;

                if (hoursRemaining > 0) {
                    countdownMessage += `${hoursRemaining} saat `;
                }

                if (minutesRemaining > 0) {
                    countdownMessage += `${minutesRemaining} dakika `;
                }

                countdownMessage += `${secondsRemaining} saniye kaldı.`;

                // Display additional messages based on the current event
                if (additionalMessage) {
                   document.getElementById("dersSuan").innerHTML = additionalMessage;
                }

                // Display the countdown message on the webpage
                document.getElementById("dersSayac").innerHTML = countdownMessage;
            }
        }

        // Set interval to update and display the countdown and messages
        setInterval(function () {
            const currentDay = new Date().getDay(); // 0 = Sunday, 1 = Monday, ..., 6 = Saturday
            const currentHour = new Date().getHours();
            const currentMinute = new Date().getMinutes();

            // Define an array of events with their target days, target hours, target minutes, messages, and additional messages
            const events = [
                
            //Haftaiçi dersler
            { days: [1, 2, 3, 4], hour: 9, minute: 15, message: "Dershanenin başlamasına", additionalMessage: "Daha dershane başlamadı." },
            { days: [1, 2, 3, 4], hour: 9, minute: 50, message: "Dersin bitmesine", additionalMessage: "Şimdi 1. ders saati." },
            { days: [1, 2, 3, 4], hour: 10, minute: 0, message: "Dersin başlamasına", additionalMessage: "Şimdi 1. teneffüs." },
            { days: [1, 2, 3, 4], hour: 10, minute: 35, message: "Dersin bitmesine", additionalMessage: "Şimdi 2. ders saati." },
            { days: [1, 2, 3, 4], hour: 10, minute: 45, message: "Dersin başlamasına", additionalMessage: "Şimdi 2. teneffüs." },
            { days: [1, 2, 3, 4], hour: 11, minute: 20, message: "Dersin bitmesine", additionalMessage: "Şimdi 3. ders saati." },
            { days: [1, 2, 3, 4], hour: 11, minute: 30, message: "Dersin başlamasına", additionalMessage: "Şimdi 3. teneffüs." },
            { days: [1, 2, 3, 4], hour: 12, minute: 5, message: "Dersin bitmesine", additionalMessage: "Şimdi 4. ders saati." },
            { days: [1, 2, 3, 4], hour: 12, minute: 10, message: "Dersin başlamasına", additionalMessage: "Şimdi 4. teneffüs." },
            { days: [1, 2, 3, 4], hour: 12, minute: 45, message: "Dersin bitmesine", additionalMessage: "Şimdi 5. ders saati." },
            { days: [1, 2, 3, 4], hour: 13, minute: 40, message: "Dersin başlamasına", additionalMessage: "Şimdi öğle teneffüsü." },
            { days: [1, 2, 3, 4], hour: 14, minute: 15, message: "Dersin bitmesine", additionalMessage: "Şimdi 6. ders saati." },
            { days: [1, 2, 3, 4], hour: 14, minute: 25, message: "Dersin başlamasına", additionalMessage: "Şimdi 6. teneffüs." },
            { days: [1, 2, 3, 4], hour: 15, minute: 0, message: "Dersin bitmesine", additionalMessage: "Şimdi 7. ders saati." },
            { days: [1, 2, 3, 4], hour: 15, minute: 10, message: "Dersin başlamasına", additionalMessage: "Şimdi 7.teneffüs." },
            { days: [1, 2, 3, 4], hour: 15, minute: 45, message: "Dersin bitmesine", additionalMessage: "Şimdi 8. ders saati." },
            { days: [1, 2, 3, 4], hour: 15, minute: 50, message: "Dersin başlamasına", additionalMessage: "Şimdi 8. teneffüs." },
            { days: [1, 2, 3, 4], hour: 16, minute: 25, message: "Dersin bitmesine", additionalMessage: "Şimdi 9. ders saati." },
            { days: [1, 2, 3, 4], hour: 16, minute: 30, message: "Dersin başlamasına", additionalMessage: "Şimdi 9. teneffüs." },
            { days: [1, 2, 3, 4], hour: 17, minute: 5, message: "Dersin bitmesine", additionalMessage: "Şimdi 10. ders saati." },
            { days: [1, 2, 3, 4], hour: 17, minute: 20, message: "Dersin başlamasına", additionalMessage: "Şimdi 10. teneffüs." },
            { days: [1, 2, 3, 4], hour: 18, minute: 5, message: "Dersin bitmesine", additionalMessage: "Şimdi 11. ders saati." },
            { days: [1, 2, 3, 4], hour: 17, minute: 55, message: "Dersin başlamasına", additionalMessage: "Şimdi 11. teneffüs." },
            { days: [1, 2, 3, 4], hour: 18, minute: 40, message: "Dersin bitmesine", additionalMessage: "Şimdi 12. ders saati." },
            { days: [1, 2, 3, 4], hour: 18, minute: 50, message: "Dersin başlamasına", additionalMessage: "Şimdi 12. teneffüs." },
            { days: [1, 2, 3, 4], hour: 19, minute: 25, message: "Dersin bitmesine", additionalMessage: "Şimdi 13. ders saati." },
            
            //Cuma deneme
            { days: [5], hour: 10, minute: 0, message: "Deneme sınavının başlamasına", additionalMessage: "Daha deneme sınavı başlamadı." },
            { days: [5], hour: 12, minute: 45, message: "Deneme sınavının bitmesine", additionalMessage: "Daha deneme sınavı  bitmedi." },
            { days: [5], hour: 23, minute: 59, message: "Deneme sınavı bitmiştir", additionalMessage: "" },

            //Haftasonu dersler
            { days: [6, 0], hour: 9, minute: 0, message: "Dershanenin başlamasına", additionalMessage: "Daha dershane başlamadı." },
            { days: [6, 0], hour: 9, minute: 35, message: "Dersin bitmesine", additionalMessage: "Şimdi 1. ders saati." },
            { days: [6, 0], hour: 9, minute: 45, message: "Dersin başlamasına", additionalMessage: "Şimdi 1. teneffüs." },
            { days: [6, 0], hour: 10, minute: 20, message: "Dersin bitmesine", additionalMessage: "Şimdi 2. ders saati." },
            { days: [6, 0], hour: 10, minute: 30, message: "Dersin başlamasına", additionalMessage: "Şimdi 2. teneffüs." },
            { days: [6, 0], hour: 11, minute: 5, message: "Dersin bitmesine", additionalMessage: "Şimdi 3. ders saati." },
            { days: [6, 0], hour: 11, minute: 15, message: "Dersin başlamasına", additionalMessage: "Şimdi 3. teneffüs." },
            { days: [6, 0], hour: 11, minute: 50, message: "Dersin bitmesine", additionalMessage: "Şimdi 4. ders saati." },
            { days: [6, 0], hour: 11, minute: 55, message: "Dersin başlamasına", additionalMessage: "Şimdi 4. teneffüs." },
            { days: [6, 0], hour: 12, minute: 30, message: "Dersin bitmesine", additionalMessage: "Şimdi 5. ders saati." },
            { days: [6, 0], hour: 12, minute: 35, message: "Dersin başlamasına", additionalMessage: "Şimdi 5 teneffüs." },
            { days: [6, 0], hour: 13, minute: 10, message: "Dersin bitmesine", additionalMessage: "Şimdi 6 ders saati." },
            { days: [6, 0], hour: 14, minute: 0, message: "Dersin başlamasına", additionalMessage: "Şimdi öğle teneffüsü." },
            { days: [6, 0], hour: 14, minute: 35, message: "Dersin bitmesine", additionalMessage: "Şimdi 7. ders saati." },
            { days: [6, 0], hour: 14, minute: 45, message: "Dersin başlamasına", additionalMessage: "Şimdi 7.teneffüs." },
            { days: [6, 0], hour: 15, minute: 20, message: "Dersin bitmesine", additionalMessage: "Şimdi 8. ders saati." },
            { days: [6, 0], hour: 15, minute: 30, message: "Dersin başlamasına", additionalMessage: "Şimdi 8. teneffüs." },
            { days: [6, 0], hour: 16, minute: 5, message: "Dersin bitmesine", additionalMessage: "Şimdi 9. ders saati." },
            { days: [6, 0], hour: 16, minute: 15, message: "Dersin başlamasına", additionalMessage: "Şimdi 9. teneffüs." },
            { days: [6 ,0], hour: 16, minute: 50, message: "Dersin bitmesine", additionalMessage: "Şimdi 10. ders saati." },
            { days: [6, 0], hour: 16, minute: 55, message: "Dersin başlamasına", additionalMessage: "Şimdi 10. teneffüs." },
            { days: [6, 0], hour: 17, minute: 30, message: "Dersin bitmesine", additionalMessage: "Şimdi 11. ders saati." },
            { days: [6, 0], hour: 17, minute: 35, message: "Dersin başlamasına", additionalMessage: "Şimdi 11. teneffüs." },
            { days: [6, 0], hour: 18, minute: 10, message: "Dersin bitmesine", additionalMessage: "Şimdi 12. ders saati." },
            ];

            // Find the first event that hasn't started yet
            const upcomingEvent = events.find(event => event.days.includes(currentDay) && (currentHour < event.hour || (currentHour === event.hour && currentMinute < event.minute)));

            // If there's an upcoming event, update the countdown
            if (upcomingEvent) {
                updateCountdownWithMessage(upcomingEvent.hour, upcomingEvent.minute, upcomingEvent.message, upcomingEvent.additionalMessage);
            } else {
                document.getElementById("dersSuan").innerHTML = "Dershane bitmiştir.";
                document.getElementById("dersSayac").innerHTML = ""; // Sayacı temizle
            }
        }, 1); // Kontrol etme süresi, 1000 = 1 saniye.