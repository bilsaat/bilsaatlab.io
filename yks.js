// Function to update and display the countdown and messages on the webpage
        function updateCountdownWithMessage(targetHour, targetMinute, message, additionalMessage) {
            const currentTime = new Date();
            const targetTime = new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), targetHour, targetMinute);
            const timeDiff = targetTime - currentTime;

            if (timeDiff > 0) {
                const hoursRemaining = Math.floor(timeDiff / (1000 * 60 * 60));
                const minutesRemaining = Math.floor((timeDiff % (1000 * 60 * 60)) / (1000 * 60));
                const secondsRemaining = Math.floor((timeDiff % (1000 * 60)) / 1000);

                let countdownMessage = `${message} `;

                if (hoursRemaining > 0) {
                    countdownMessage += `${hoursRemaining} saat `;
                }

                if (minutesRemaining > 0) {
                    countdownMessage += `${minutesRemaining} dakika `;
                }

                countdownMessage += `${secondsRemaining} saniye kaldı.`;

                // Display additional messages based on the current event
                if (additionalMessage) {
                    document.getElementById("yksSuan").innerHTML = additionalMessage;
                }

                // Display the countdown message on the webpage
                document.getElementById("yksSayac").innerHTML = countdownMessage;
            }
        }

        // Set interval to update and display the countdown and messages
        setInterval(function () {
            const currentDay = new Date().getDay(); // 0=Pazar 1=Pazartesi 2=Salı 3=Çarşamda 4=Perşembe 5=Cuma 6=Cumartesi
            const currentHour = new Date().getHours();
            const currentMinute = new Date().getMinutes();

            // Define an array of events with their target days, target hours, target minutes, messages, and additional messages
            const events = [

            // TYT Denemeler
            { days: [1, 3, 5], hour: 10, minute: 0, message: "TYT Denemesinin başlamasına", additionalMessage: "Daha TYT Denemesi başlamadı." },
            { days: [1, 3, 5], hour: 12, minute: 45, message: "TYT Denemesinin bitmesine", additionalMessage: "Şimdi TYT Denemesi var!" },

          // AYT/YDT Denemeler
          { days: [2, 4, 6], hour: 10, minute: 0, message: "AYT/YDT Denemesinin başlamasına", additionalMessage: "Daha AYT/YDT Denemesi başlamadı." },
          { days: [2, 4, 6], hour: 12, minute: 0, message: "AYT/YDT Denemesinin bitmesine", additionalMessage: "Şimdi AYT/YDT Denemesi var!" },
          { days: [2, 4, 6], hour: 13, minute: 0, message: " AYT Denemesinin bitmesine", additionalMessage: "YDT Denemesi bitti, <br> AYT Denemesi hâlâ devam ediyor!" },
          
            // Deneme sonrası kütüphaneler
            { days: [1, 2, 3, 4, 5], hour: 13, minute: 40, message: "Kütüphanenin başlamasına", additionalMessage: "Şimdi öğle teneffüsü." },
            { days: [1, 2, 3, 4, 5], hour: 14, minute: 40, message: "Kütüphanenin bitmesine", additionalMessage: "Şimdi kütüphane saati." },
            { days: [1, 2, 3, 4, 5], hour: 14, minute: 50, message: "Kütüphanenin başlamasına", additionalMessage: "Şimdi teneffüs." },
            { days: [1, 2, 3, 4, 5], hour: 15, minute: 50, message: "Kütüphanenin bitmesine", additionalMessage: "Şimdi kütüphane saati." },
            { days: [1, 2, 3, 4, 5], hour: 16, minute: 0, message: "Kütüphanenin başlamasına", additionalMessage: "Şimdi teneffüs." },
            { days: [1, 2, 3, 4, 5], hour: 17, minute: 5, message: "Kütüphanenin bitmesine", additionalMessage: "Şimdi kütüphane saati." },
            { days: [1, 2, 3, 4, 5], hour: 17, minute: 20, message: "Kütüphanenin başlamasına", additionalMessage: "Şimdi teneffüs." },
            { days: [1, 2, 3, 4, 5], hour: 18, minute: 20, message: "Kütüphanenin bitmesine", additionalMessage: "Şimdi kütüphane saati." },
            { days: [1, 2, 3, 4, 5], hour: 18, minute: 30, message: "Kütüphanenin başlamasına", additionalMessage: "Şimdi teneffüs." },
            { days: [1, 2, 3, 4, 5], hour: 19, minute: 25, message: "Kütüphanenin bitmesine", additionalMessage: "Şimdi kütphane saati." },
            
            //Tatil
            { days: [0], hour: 23, minute: 59, message: "Dershane bugün yok, ama başlamasına", additionalMessage: "Bugün Pazar." }, ];

            // Find the first event that hasn't started yet
            const upcomingEvent = events.find(event => event.days.includes(currentDay) && (currentHour < event.hour || (currentHour === event.hour && currentMinute < event.minute)));

            // If there's an upcoming event, update the countdown
            if (upcomingEvent) {
                updateCountdownWithMessage(upcomingEvent.hour, upcomingEvent.minute, upcomingEvent.message, upcomingEvent.additionalMessage);
            } else {
                // Sayaç bitince bu mesajı görüntüle
                document.getElementById("yksSuan").innerHTML = "Dershane bitti.";
                document.getElementById("yksSayac").innerHTML = "";
            }
        }, 1); // Kontrol etme süresi, 1000 = 1 saniye.