// Function to check if the device is a mobile device
function isMobileDevice() {
    return /Mobi|Android|iPhone/i.test(navigator.userAgent);
  }
  
  // Check if the device is mobile before showing the label and checkbox
  if (isMobileDevice()) {
    document.getElementById('enableWakeLock').style.display = 'inline'; // Show the checkbox
    document.getElementById('labelForMobile').style.display = 'inline'; // Show the label
  } else {
    console.log('Not a mobile device. Screen Wake Lock feature not available.');
  }
  
  const enableWakeLockCheckbox = document.getElementById('enableWakeLock');
  
  enableWakeLockCheckbox.addEventListener('change', () => {
    if (enableWakeLockCheckbox.checked) {
      // Request wake lock when checkbox is checked
      navigator.wakeLock.request('screen')
        .then((wakeLockObj) => {
          console.log('Screen wake lock is active');
        })
        .catch((error) => {
          console.error('Error requesting wake lock: ', error);
        });
    } else {
      // Release wake lock when checkbox is unchecked
      navigator.wakeLock.release();
      console.log('Screen wake lock released');
    }
  });
  